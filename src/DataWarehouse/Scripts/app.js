'use strict';

/*****
* CONFIGURATION
*/
	// Active ajax page loader
	$.ajaxLoad = true;

	//required when $.ajaxLoad = true
	$.defaultPage = 'ToDoList/Index';
	$.subPagesDirectory = '/';
	$.page404 = '#Home/Missing';
	$.mainContent = $('#ui-view');

    //Main navigation
    $.navigation = $('nav > ul.nav');

	$.panelIconOpened = 'icon-arrow-up';
	$.panelIconClosed = 'icon-arrow-down';

	//Default colours
	$.brandPrimary =  '#20a8d8';
	$.brandSuccess =  '#4dbd74';
	$.brandInfo =     '#63c2de';
	$.brandWarning =  '#f8cb00';
	$.brandDanger =   '#f86c6b';

	$.grayDark =      '#2a2c36';
	$.gray =          '#55595c';
	$.grayLight =     '#818a91';
	$.grayLighter =   '#d1d4d7';
	$.grayLightest =  '#f8f9fa';

	$.ajaxSetup({
	    statusCode: {
	        401: function () {
                window.location.href = "";
	        },
            403: function() {
                window.location.href = "";
            }
	    }
    });

    $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
        // Looks like this is azure auth expired and it failed due to CORS violation
        // Need to refresh page to authentication on server side
        if (jqxhr.status === 0 && jqxhr.statusText === "error" && jqxhr.readyState === 0) {
            sessionStorage.setItem("currentPage", window.location.hash);
            window.location.reload(true);
        }
    });


/*****
* ASYNC LOAD
* Load JS files and CSS files asynchronously in ajax mode
*/
function loadJS(jsFiles, pageScript) {

  var i;
  for(i = 0; i<jsFiles.length;i++){

    var body = document.getElementsByTagName('body')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = false;
    script.src = jsFiles[i];
    body.appendChild(script);
  }

  if (pageScript) {
    var body = document.getElementsByTagName('body')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = false;
    script.src = pageScript;
    body.appendChild(script);
  }

  init();
}

function loadCSS(cssFile, end, callback) {

  var cssArray = {};

  if (!cssArray[cssFile]) {
    cssArray[cssFile] = true;

    if (end == 1) {

      var head = document.getElementsByTagName('head')[0];
      var s = document.createElement('link');
      s.setAttribute('rel', 'stylesheet');
      s.setAttribute('type', 'text/css');
      s.setAttribute('href', cssFile);

      s.onload = callback;
      head.appendChild(s);

    } else {

      var head = document.getElementsByTagName('head')[0];
      var style = document.getElementById('main-style');

      var s = document.createElement('link');
      s.setAttribute('rel', 'stylesheet');
      s.setAttribute('type', 'text/css');
      s.setAttribute('href', cssFile);

      s.onload = callback;
      head.insertBefore(s, style);

    }

  } else if (callback) {
    callback();
  }

}

function setUpUrl(url) {

    activateMenuItem(url);
    loadPage(url);
}

function activateMenuItem(url) {

    $('.nav li .nav-link').removeClass('active');
    $('.nav li.nav-dropdown').removeClass('open');
    $('.nav li:has(a[href="' + url.split('?')[0] + '"])').addClass('open');
    $('.nav a[href="' + url.split('?')[0] + '"]').addClass('active');
}

function loadPage(url) {
    console.log("load: " + url);
    $.mainContent.trigger("changing_page");

    LoadingPanel.Show();
    $.ajax({
        type : 'GET',
        url : $.subPagesDirectory + url,
        dataType : 'html',
        cache : false,
        async: true,
        beforeSend : function() {
            $.mainContent.css({ opacity : 0 });
        },
        success: function (data) {
            Pace.restart();
            $('html, body').animate({ scrollTop: 0 }, 0);
            $.mainContent.html(data).delay(250).animate({ opacity: 1 }, 0);
            window.location.hash = url;
            updateBreadcrumbs(url);
            ga('set', 'page', url);
            ga('send', 'pageview');
            setTimeout(function () { LoadingPanel.Hide(); }, 500);
        },
        error: function (response) {
            if (response.status == 401) {
                window.location.hash = url;
            }
            else {
                window.location.href = $.page404;
            }
            LoadingPanel.Hide();
        }
    });
}


function updateBreadcrumbs(url) {
    url = url.split('?')[0];
    var components = url.split("/");

    var element = $("#breadcrumb");
    element.empty();
    var $comp = $('<li class="breadcrumb-item">Home</li>');
    element.append($comp);

    var activeMenu = $('.nav a[href="' + url + '"]');
    if (activeMenu.length > 0) {
        // Build breadcrumbs by menu parts
        try {
            var components2 = [];
            var menuItems = activeMenu.parents('li.nav-item');
            menuItems.each(function (index, item) {
                var link = $(item).children('a');
                if (link.length > 0) {
                    var name = link.text().trim();
                    components2.unshift(name);
                }
            });

            components = components2;
        } catch (er) {
            console.error(er);
        }
    }

    for (var i = 0; i < components.length; i++) {
        var component = components[i];
        $comp = $('<li class="breadcrumb-item">' + component + '</li>');
        element.append($comp);
    }

    var pageName = $('#_PageName_');
    if (pageName && pageName.val()) {
        $comp = $('<li class="breadcrumb-item">' + pageName.val() + '</li>');
        element.append($comp);
    }
}

function submitForm(url, method, formid) {
    var data = $("#" + formid).serialize(); // serializes the form's elements.
    goToAction(url, method, data);
}

function goToAction(url, method, actionData, urlToSet) {
    console.log("load: " + url);
    $.mainContent.trigger("changing_page");

    if (!urlToSet)
        urlToSet = url;

    LoadingPanel.Show();
    $.ajax({
        type: method,
        url: $.subPagesDirectory + url,
        data: actionData,
        dataType: 'html',
        cache: false,
        async: true,
        beforeSend: function () {
            $.mainContent.css({ opacity: 0 });
        },
        complete: function (data) {
            Pace.restart();
            $('html, body').animate({ scrollTop: 0 }, 0);
            $.mainContent.html(data.responseText).delay(250).animate({ opacity: 1 }, 0);
            window.location.hash = urlToSet;
            ga('set', 'page', urlToSet);
            ga('send', 'pageview');
            updateBreadcrumbs(urlToSet);
            activateMenuItem(urlToSet);
            setTimeout(function () { LoadingPanel.Hide(); }, 500);
        },
        error: function (response) {
            if (response.status == 401) {
                window.location.hash = urlToSet;
            }
            else {
                window.location.href = $.page404;
            }
            LoadingPanel.Hide();
        }
    });
}

/****
* MAIN NAVIGATION
*/

$(document).ready(function($){

  // Add class .active to current link - AJAX Mode off
  $.navigation.find('a').each(function(){

    var cUrl = String(window.location).split('?')[0];

    if (cUrl.substr(cUrl.length - 1) == '#') {
      cUrl = cUrl.slice(0,-1);
    }

    if ($($(this))[0].href==cUrl) {
      $(this).addClass('active');

      $(this).parents('ul').add(this).each(function(){
        $(this).parent().addClass('open');
      });
    }
  });

  // Dropdown Menu
  $.navigation.on('click', 'a', function(e){

    if ($.ajaxLoad) {
      e.preventDefault();
    }

    if ($(this).hasClass('nav-dropdown-toggle')) {
      $(this).parent().toggleClass('open');
      resizeBroadcast();
    }
  });

  function resizeBroadcast() {

    var timesRun = 0;
    var interval = setInterval(function(){
      timesRun += 1;
      if(timesRun === 5){
        clearInterval(interval);
      }
      window.dispatchEvent(new Event('resize'));
    }, 62.5);
  }

  /* ---------- Main Menu Open/Close, Min/Full ---------- */
  $('.navbar-toggler').click(function(){

    if ($(this).hasClass('sidebar-toggler')) {
      $('body').toggleClass('sidebar-hidden');
      resizeBroadcast();
    }

    if ($(this).hasClass('aside-menu-toggler')) {
      $('body').toggleClass('aside-menu-hidden');
      resizeBroadcast();
    }

    if ($(this).hasClass('mobile-sidebar-toggler')) {
      $('body').toggleClass('sidebar-mobile-show');
      resizeBroadcast();
    }

  });

  $('.sidebar-close').click(function(){
    $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
  });

  /* ---------- Disable moving to top ---------- */
  $('a[href="#"][data-top!=true]').click(function(e){
    e.preventDefault();
  });

  if ($.ajaxLoad) {
      var paceOptions = {
          elements: false,
          restartOnRequestAfter: false
      };


      // Initial page is currenlty set on MVCxClientGlobalEvents.AddControlsInitializedEventHandler 
      //var url = location.hash.replace(/^#/, '');
      //if (url != '') {
      //    setUpUrl(url);
      //} else {
      //    setUpUrl($.defaultPage);
      //}
      

      $(document).on('click', '.nav a[href!="#"]', function (e) {
          if ($(this).attr('id') == 'RefreshSession') {
              e.preventDefault();
              var target = $(e.currentTarget);
              console.log("load: " + target.attr('href'));
              $.ajax({
                  type: 'GET',
                  dataType: 'json',
                  url: target.attr('href'),
                  cache: false,
                  async: true,
                  success: function(data) {
                      window.SessionTime = data.SessionTime;
                  }
              });
          } else if ($(this).parent().parent().hasClass('nav-tabs') || $(this).parent().parent().hasClass('nav-pills')) {
              e.preventDefault();
          } else if ($(this).attr('target') == '_top') {
              e.preventDefault();
              var target = $(e.currentTarget);
              window.location = (target.attr('href'));
          } else if ($(this).attr('target') == '_blank') {
              e.preventDefault();
              var target = $(e.currentTarget);
              window.open(target.attr('href'));
          } else {
              e.preventDefault();
              var target = $(e.currentTarget);
              setUpUrl(target.attr('href'));
          }
      });

      $(document).on('click', 'a[href="#"]', function (e) {
          e.preventDefault();
      });

      $(document).ajaxSuccess(function () {
          $('#sessionTime').countdown(new Date().getTime() + window.SessionTime);
      });
      
      $('#sessionTime').countdown(new Date().getTime() + window.SessionTime, { elapse: false })
          .on('finish.countdown', function () {
              alert("Your session has expired. Please log in again.");
              window.location = "/Account/LogOff";
          }).on('update.countdown', function (event) {
              var $this = $(this);
              $this.html(event.strftime('%H:%M:%S'));
          });
  }
});

/****
* CARDS ACTIONS
*/

$(document).on('click', '.card-actions a', function(e){
  e.preventDefault();

  if ($(this).hasClass('btn-close')) {
    $(this).parent().parent().parent().fadeOut();
  } else if ($(this).hasClass('btn-minimize')) {
    var $target = $(this).parent().parent().next('.card-block');
    if (!$(this).hasClass('collapsed')) {
      $('i',$(this)).removeClass($.panelIconOpened).addClass($.panelIconClosed);
    } else {
      $('i',$(this)).removeClass($.panelIconClosed).addClass($.panelIconOpened);
    }

  } else if ($(this).hasClass('btn-setting')) {
    $('#myModal').modal('show');
  }

});

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function init(url) {

  /* ---------- Tooltip ---------- */
  $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});

  /* ---------- Popover ---------- */
  $('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();

}
