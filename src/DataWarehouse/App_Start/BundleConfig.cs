using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace DataWarehouse {

    public class BundleConfig {

        public static void RegisterBundles(BundleCollection bundles) {

            var scriptBundle = new ScriptBundle("~/Scripts/bundle");
            var styleBundle = new StyleBundle("~/Content/bundle");

            // jQuery
            scriptBundle
                .Include("~/Scripts/jquery-2.2.3.js");

            // Bootstrap
            scriptBundle
                .Include("~/Scripts/bootstrap.js");

            // Bootstrap
            styleBundle
                .Include("~/Content/bootstrap.css");

            // Custom site styles
            styleBundle
                .Include("~/Content/Site.css");

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/font-awesome.min.css",
                "~/Content/simple-line-icons.css",
                "~/Content/glyphicons.css",
                "~/Content/glyphicons-filetypes.css",
                "~/Content/glyphicons-social.css",
                "~/Content/jquery-ui.min.css",
                "~/Content/style.css"));
            bundles.Add(scriptBundle);
            bundles.Add(styleBundle);

#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}