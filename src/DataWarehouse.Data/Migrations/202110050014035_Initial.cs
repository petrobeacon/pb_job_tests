namespace DataWarehouse.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ToDoListItem",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ToDoListId = c.Guid(nullable: false),
                        CreatedBy = c.String(maxLength: 200),
                        CreateDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 200),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ToDoList", t => t.ToDoListId, cascadeDelete: true)
                .Index(t => t.ToDoListId);
            
            CreateTable(
                "dbo.ToDoList",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        CreatedBy = c.String(maxLength: 200),
                        CreateDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 200),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ToDoListItem", "ToDoListId", "dbo.ToDoList");
            DropIndex("dbo.ToDoListItem", new[] { "ToDoListId" });
            DropTable("dbo.ToDoList");
            DropTable("dbo.ToDoListItem");
        }
    }
}
