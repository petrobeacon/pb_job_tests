﻿using System.Web.Mvc;

namespace DataWarehouse.Controllers
{
    public class BaseController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }
    }
}