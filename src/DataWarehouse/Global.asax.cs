using DataWarehouse.Common.Data;
using DevExpress.Web;
using NLog;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Integration.WebApi;
using System;
using System.IdentityModel.Claims;
using System.Reflection;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DataWarehouse
{

    public class MvcApplication : HttpApplication
    {
        private Logger _logger;
        protected void Application_Start()
        {
            _logger = InitializeLogger();
            RegisterContainer();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            DevExtremeBundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
            ModelBinders.Binders.DefaultBinder = new DevExpress.Web.Mvc.DevExpressEditorsBinder();
        }

        private Container RegisterContainer()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            InitializeApplicationDbContext(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.RegisterMvcIntegratedFilterProvider();
            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
            return container;
        }

        private void InitializeApplicationDbContext(Container container)
        {
            container.Register<DataWarehouseDbContext>(DataWarehouseDbContext.Create, Lifestyle.Scoped);
        }

        private Logger InitializeLogger()
        {
            ASPxWebControl.CallbackError += Application_Error;
            LogManager.ThrowExceptions = true;
            var logger = LogManager.GetCurrentClassLogger();
            logger.Info("PetroBeacon DemoApp " + GetType().Assembly.GetName().Version);
            return logger;
        }

        private void Application_Error(object sender, EventArgs e)
        {
            HttpServerUtility server = HttpContext.Current.Server;
            Exception exception = server.GetLastError();
            if (_logger == null) _logger = InitializeLogger();
            if (exception is HttpUnhandledException) exception = exception.InnerException;
            if (exception != null && _logger != null)
            {
                _logger.Error(exception, "Unexpected exception occured");
            }
        }
    }
}
