﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataWarehouse.Common.Data.Model
{
    public abstract class IdModelItem
    {
        [Key]
        public Guid Id { get; set; }
    }
}
