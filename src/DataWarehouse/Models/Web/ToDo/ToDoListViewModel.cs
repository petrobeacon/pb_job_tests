﻿using DataWarehouse.Common.Data.Model.TractData;
using System;
using System.ComponentModel.DataAnnotations;

namespace DataWarehouse.Models.Web.ToDo
{
    public class ToDoListViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "List Name")]
        public string Name { get; set; }
        
        [Display(Name = "Description")]
        public string Description { get; set; }

        public ToDoListViewModel() { }

        public ToDoListViewModel(ToDoList todoList)
        {
            Id = todoList.Id;
            Name = todoList.Name;
            Description = todoList.Description;
        }
    }
}