using System.Web.Mvc;
using DataWarehouse.Common.Data;


namespace DataWarehouse.Controllers {
    public class HomeController : BaseController
    {
        private readonly DataWarehouseDbContext _context;

        public HomeController(DataWarehouseDbContext context)
        {
            _context = context;
        }

        public ActionResult Missing()
        {
            return View();
        }

        public ActionResult SidebarPartial()
        {
            return PartialView("_SidebarPartial");
        }
    }
}