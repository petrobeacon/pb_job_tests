﻿using DataWarehouse.Common.Data.Model.TractData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataWarehouse.Common.Data.Repository
{
    public class ToDoListRepository
    {
        private readonly DataWarehouseDbContext _context;
        public ToDoListRepository(DataWarehouseDbContext context)
        {
            _context = context;
        }

        public List<ToDoList> GetToDoLists()
        {
            return _context.ToDoLists.OrderBy(x => x.Name).ToList();
        }

        public ToDoList GetToDoList(Guid id)
        {
            return _context.ToDoLists.Find(id);
        }

        public ToDoList ToDoList(string name)
        {
            return _context.ToDoLists.FirstOrDefault(x => String.Equals(name, x.Name));
        }

        public async Task<ToDoList> AddToDoList(string name, string description)
        {
            var added = _context.ToDoLists.Add(new ToDoList()
            {
                Id = Guid.NewGuid(),
                Name = name,
                Description = description
            });
            DataUtility.SetSystemAttributes(_context, added, true);
            await _context.SaveChangesAsync();
            return added;
        }

        public async Task UpdateToDoList(Guid id, string name, string description)
        {
            var toUpdate = _context.ToDoLists.FirstOrDefault(x => x.Id == id);
            if (toUpdate != null)
            {
                toUpdate.Name = name;
                toUpdate.Description = description;
            }
            DataUtility.SetSystemAttributes(_context, toUpdate, false);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveToDoList(Guid id)
        {
            var toDelete = _context.ToDoLists.FirstOrDefault(x => x.Id == id);
            if (toDelete != null) _context.ToDoLists.Remove(toDelete);

            await _context.SaveChangesAsync();
        }

        public bool IsDuplicateToDoList(string name, Guid? dbId)
        {
            return _context.ToDoLists.Any(x => String.Equals(name, x.Name) && (!dbId.HasValue || x.Id != dbId.Value));
        }
    }
}
