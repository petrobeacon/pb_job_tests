﻿using DataWarehouse.Common.Data;
using DataWarehouse.Common.Data.Repository;
using DataWarehouse.Models.Web.ToDo;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataWarehouse.Controllers
{
    public class ToDoListController : BaseController
    {
        private DataWarehouseDbContext _context;
        private readonly ToDoListRepository _lookupRepository;

        public ToDoListController(DataWarehouseDbContext context)
        {
            _context = context;
            _lookupRepository = new ToDoListRepository(context);
        }

        private object GetViewModel()
        {
            return _lookupRepository.GetToDoLists().Select(x => new ToDoListViewModel(x)).ToList();
        }

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", GetViewModel());
        }

        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> GridViewPartialUpdate(ToDoListViewModel item)
        {
            if (ModelState.IsValid)
            {
                if (_lookupRepository.IsDuplicateToDoList(item.Name, item.Id))
                    ModelState.AddModelError("Name", "Duplicated list, list name must be unique");
                else
                    await _lookupRepository.UpdateToDoList(item.Id, item.Name, item.Description);
            }
            else ViewData["EditError"] = "Please, correct all errors.";
            return GridViewPartial();
        }

        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> GridViewPartialDelete(Guid id)
        {
            if (id != null)
            {
                await _lookupRepository.RemoveToDoList(id);
            }
            return GridViewPartial();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(ToDoListViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_lookupRepository.IsDuplicateToDoList(model.Name, null))
                        ModelState.AddModelError("Name", "Duplicate list, list name must be unique");
                    else
                        await _lookupRepository.AddToDoList(model.Name, model.Description);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null) e = e.InnerException;
                ModelState.AddModelError(String.Empty, e.Message);
            }

            return View("Index");
        }
    }
}