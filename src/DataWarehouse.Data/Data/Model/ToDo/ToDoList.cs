﻿using System;
using System.Collections.Generic;

namespace DataWarehouse.Common.Data.Model.TractData
{
    public class ToDoList : BaseModelItem
    {
        public String Name { get; set; }
        public String Description { get; set; }

        public virtual ICollection<ToDoListItem> Items { get; set; }

        public ToDoList()
        {
            Items = new HashSet<ToDoListItem>();
        }
    }
}
