﻿using Microsoft.Owin;
using Owin;
using NLog;
using Microsoft.IdentityModel.Logging;
using Microsoft.Azure.ActiveDirectory.GraphClient;

[assembly: OwinStartup(typeof(DataWarehouse.Startup))]

namespace DataWarehouse
{
    public class Startup
    {
        public static readonly Logger Logger = LogManager.GetLogger(typeof(Startup).Name);

        public void Configuration(IAppBuilder app)
        {
        }

        public void ConfigureServices(IDeviceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;
        }
    }
}
