﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using DataWarehouse.Common.Data.Model.TractData;
using NLog;

namespace DataWarehouse.Common.Data
{
    public class DataWarehouseDbContext : DbContext, IDisposable
    {
        private static readonly Logger Logger = LogManager.GetLogger(typeof(DataWarehouseDbContext).Name);

        public DataWarehouseDbContext() : base("name=DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ToDoList>().ToTable("ToDoList");
            modelBuilder.Entity<ToDoListItem>().ToTable("ToDoListItem");

            modelBuilder.Entity<ToDoListItem>().HasRequired(x => x.ToDoList).WithMany(x => x.Items).WillCascadeOnDelete(true);
        }

        public virtual DbSet<ToDoList> ToDoLists { get; set; }
        public virtual DbSet<ToDoListItem> ToDoListItems { get; set; }

        public static DataWarehouseDbContext Create()
        {
            return new DataWarehouseDbContext();
        }

        public ObjectContext ObjectContext
        {
            get { return ((IObjectContextAdapter)this).ObjectContext; }
        }
    }
}
