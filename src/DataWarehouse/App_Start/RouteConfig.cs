using System.Web.Mvc;
using System.Web.Routing;

namespace DataWarehouse {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "",
                namespaces: new[] { "DataWarehouse.Controllers" },
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "View",
                url: "{controller}/{action}/{id}",
                namespaces: new[] { "DataWarehouse.Controllers" },
                defaults: new { controller = "Home", action = "Missing", id = UrlParameter.Optional }
            );
        }
    }
}
