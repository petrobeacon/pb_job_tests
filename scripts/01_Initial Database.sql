﻿USE [PB_DEMO]
DECLARE @CurrentMigration [nvarchar](max)

IF object_id('[dbo].[__MigrationHistory]') IS NOT NULL
    SELECT @CurrentMigration =
        (SELECT TOP (1) 
        [Project1].[MigrationId] AS [MigrationId]
        FROM ( SELECT 
        [Extent1].[MigrationId] AS [MigrationId]
        FROM [dbo].[__MigrationHistory] AS [Extent1]
        WHERE [Extent1].[ContextKey] = N'DataWarehouse.Common.Migrations.Configuration'
        )  AS [Project1]
        ORDER BY [Project1].[MigrationId] DESC)

IF @CurrentMigration IS NULL
    SET @CurrentMigration = '0'

IF @CurrentMigration < '202110050014035_Initial'
BEGIN
    CREATE TABLE [dbo].[ToDoListItem] (
        [Id] [uniqueidentifier] NOT NULL,
        [Name] [nvarchar](max),
        [ToDoListId] [uniqueidentifier] NOT NULL,
        [CreatedBy] [nvarchar](200),
        [CreateDate] [datetime],
        [UpdatedBy] [nvarchar](200),
        [UpdateDate] [datetime],
        CONSTRAINT [PK_dbo.ToDoListItem] PRIMARY KEY ([Id])
    )
    CREATE INDEX [IX_ToDoListId] ON [dbo].[ToDoListItem]([ToDoListId])
    CREATE TABLE [dbo].[ToDoList] (
        [Id] [uniqueidentifier] NOT NULL,
        [Name] [nvarchar](max),
        [Description] [nvarchar](max),
        [CreatedBy] [nvarchar](200),
        [CreateDate] [datetime],
        [UpdatedBy] [nvarchar](200),
        [UpdateDate] [datetime],
        CONSTRAINT [PK_dbo.ToDoList] PRIMARY KEY ([Id])
    )
    ALTER TABLE [dbo].[ToDoListItem] ADD CONSTRAINT [FK_dbo.ToDoListItem_dbo.ToDoList_ToDoListId] FOREIGN KEY ([ToDoListId]) REFERENCES [dbo].[ToDoList] ([Id]) ON DELETE CASCADE
    CREATE TABLE [dbo].[__MigrationHistory] (
        [MigrationId] [nvarchar](150) NOT NULL,
        [ContextKey] [nvarchar](300) NOT NULL,
        [Model] [varbinary](max) NOT NULL,
        [ProductVersion] [nvarchar](32) NOT NULL,
        CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY ([MigrationId], [ContextKey])
    )
    INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion])
    VALUES (N'202110050014035_Initial', N'DataWarehouse.Common.Migrations.Configuration',  0x1F8B0800000000000400ED5ACD6EE33610BE17E83B083AB545D68A772FAD61EF226B278BA049BC889D6D6F012D8D6DA214A925A9C046D127EBA18FD457E850B26449946D3976822C1A0408246AE6E3CC70FE38C9BF7FFFD3FDB00899F3005251C17B6EBB75EA3AC07D11503EEBB9B19EBEF9D9FDF0FEFBEFBAE741B870BE6474EF0C1D7272D573E75A471DCF53FE1C42A25A21F5A55062AA5BBE083D1208EFEDE9E92F5EBBED0142B888E538DDDB986B1A42F282AF7DC17D88744CD8B50880A9D53A7E1925A8CE0D094145C4879E3B209AFC4624CC45ACA0D517612878CB2CBACE19A304051A019BBA0EE15C68A251DCCE9D82919682CF46112E10365E46807453C214ACD4E8ACC99B6A74FAD668E4AD1933283F565A847B02B6DFAD4CE455D91F65683737211AF11C8DAD9746EBC4903D772C06E28A2A7DA921749DEA869D3E9386788BAD5BC939B5C692F83A792F229E38757C27B9F7A093999F13A71F331D4BE87188B524ECC4F91C4F18F57F85E558FC01BCC763C68A8AA02AF8ADB4804B9FA58840EAE52D4C57EA5D06AEE395F9BC2A63CE56E04995FE14537CBEC1BDC98441EE26DE5676F33B03405FC3E8719D6BB2B8023ED3F39E8B8FAE73411710642B2BD43B4E31D89049CB78E726B9910F93B52F8168083E2EB7088CAE740481D39DD01B72DB98E7310D7772DE45C133C998EED448C61BF2406749886C3818D7B9059610A8398DD24C540A8DFB35E58514E1AD609568CC09EE472296BE91496CA31A1339035D96B4EBAD03BE511A386E0A780DFF270AFF01285FD228AD094FBCD76B8E38768E3071AB8E9920B2D0DF9E20B234D224419C29257C9A8857D328AC452B6B7CCE03A751224BCFA8AA311E17E6011A61E4A3483DF727CBA4BB36C80D616F50056FBBD5F431E40360A0C139F3D35EAD4F944F02FB60D166417905330E48E0A6A3C40656610EA35CDBE989729F46843551A1C2DC30BB19E1F26DAA5F0610010F50CA2667D464FF621362CB916F5731DE2E5B75BD82F3D9450B79347280CC5261B1C20C26E6332CEAEA18B6FEAB52A656A15EF52D033F025DE3F018ADEB38A9F55CCB51EBC1B6015920053BD48B55481105DAADB9A47A4C4D433657C8328C75F24D83B406D2822B390392D6A4AEDC21D657452FBD2B66774A6FC3A5B27B4DA208CB44E192B95A7146E90DB3FF66B4FF9D2B4C313C5FD55CBD7269F39DB490640695AFB8354A7A41A54A3AAA093185AA1F8416D906F7DFE08DD9A6751E6E1F63E6A7199779AE09BA522B588A0A3B89ACA02E50E5D06422A33D6CF02D9B3D99021046644DCBD6172C0EF9E6C4B8993BEDD88AFCE94A738462222CE26C4B909BD10A0D5711ACB0BC2F56DAAED860E97A73B4429B55042B2CEF8B654B565CB7D1BA5EC583ACB263B9AD55BBCBB1B057A41C3B4A0E88906F273A4A97952250E9C36B7CBCF8F8B02A6E9524DF3DAFBC950ADB5D55BBDDB35DABFCA524AE83667AA081297DA3A5C22A9106D4E82BEB338AFAAE09AE09A753503A9D20E0F5AFFDB632177E39335A4FA980ED31A87DF65148CCE9D718A869E7E994823C682CC21F88F4E744FE1092C58F45A4C74D3E6DD9ACDEFF122F228B9EFB6782D1712E7FBF5FC39C384389EED2714E9DBF0E1FA066BAD9A387FDD4B4672126EC753267D80FC99A8D1C4B447B14D24CC4FD8792FF6377AF99F41D84F7EAB0C773D8630DC99E7F5E557F453F603077D000ECB9875DDFCE70CB1E02349F59ED1C59A51D1506C144E0C9A7CEFFA8A1D6D699D6B65D5EC8C4AB7614D578B0B57136F634B32CBB8346AF2BFC0F057ABEA2B33584F98F0A0E7EC9DF729A4B3E1599EB5724CA482A39F41A34C1AC49CE245646E26BFCEC8352C91F5BBE101623C9793881E0920F631DC51A558670C24A571F133EDBF64F06766599BBC3A404AA63A880625293F887FC634C5990CB7D6117F64D10262E3F01AEA7678961AE61B6CC916E046F08B4325F9E4EC610460CC1D4908FC8033C46B63B05573023FE32BB086D06D97D1065B3770794CC2409D50A63CD8FAFE8C341B878FF1F0115C0DE4A240000 , N'6.2.0-61023')
END

