﻿using DataWarehouse.Common.Data.Model;
using System;

namespace DataWarehouse.Common.Data
{
    public static class DataUtility
    {
        public static void SetSystemAttributes(DataWarehouseDbContext context, BaseModelItem item, bool newItem = true)
        {
            SetSystemAttributes(context, Guid.NewGuid(), item, newItem);
        }

        public static void SetSystemAttributes(DataWarehouseDbContext context, Guid id, BaseModelItem item, bool newItem = true)
        {
            if(newItem)
            {
                item.Id = id;
                item.CreateDate = DateTime.UtcNow;
            }
            item.UpdateDate = DateTime.UtcNow;
        }
    }
}
