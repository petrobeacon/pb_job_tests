﻿function saveToUrl(key, value) {
    var uri = location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    var newParameterValue = value ? key + "=" + encodeURIComponent(value) : "";
    var newUrl;
    if (uri.match(re)) {
        var separator = !!newParameterValue ? '$1' : "";
        newUrl = uri.replace(re, separator + newParameterValue + '$2');
    }
    else if (!!newParameterValue) {
        newUrl = uri + separator + newParameterValue;
    }
    if (newUrl) {
        history.replaceState({}, "", newUrl);
    }
}

function onCustomizeMenuItems(designer, eventArgs) {
    if (!designer.cpIsSqlExpressInstalled) {
        var itemNew = eventArgs.FindById('new');
        if (itemNew) {
            itemNew.template = 'dx-dshd-form-new-disallowed';
        }
    }

}

var EnsureDashboardSaved = function (e) {
    $.mainContent.off('changing_page', EnsureDashboardSaved);

    var dashboardControl = DashboardViewer.getDashboardControl();
    var saveExtension = dashboardControl.findExtension("save-dashboard");

    var isDirty = saveExtension._isDashboardDirty();
    if (isDirty && confirm("Do you want to save dashboard?")) {
        saveExtension.saveDashboard();
    }
};

function onBeforeRender(sender, eventArgs) {
    if (sender) {
        var dashboardControl = sender.getDashboardControl();
        saveToUrl("mode", sender.GetWorkingMode());
        dashboardControl.isDesignMode.subscribe(function (value) {
            saveToUrl("mode", sender.GetWorkingMode());
        });

        $.mainContent.on('changing_page', EnsureDashboardSaved);
    }
}

function onDashboardChanged(sender, args) {
    var dashboardId = args.DashboardId,
    dashboardControl = sender.getDashboardControl();
    if (dashboardId === "CustomItemExtensions") {
        !dashboardControl.findExtension("save-as") && dashboardControl.registerExtension(new SaveAsDashboardExtension(dashboardControl));
    } else {
        dashboardControl.unregisterExtension("save-as");
    }
}

function ItemWidgetCreated(s, args) {
    if (args.GetWidget().NAME === "dxDataGrid") {
        
        var grid = args.GetWidget();
        var columns = grid.option('columns');

        for (var i in columns) {
            if (columns[i].caption == "Modified") {
                columns[i].sortIndex = 0;
                columns[i].sortOrder = 'desc';
                break;
            }
        }

        grid.option('columns', columns);
    }

    if (args.GetWidget().NAME === "dxPivotGrid") {

        var pivotGrid = args.GetWidget();
        pivotGrid.option({
            allowSorting: false,
            fieldPanel: {
                showColumnFields: false,
                showDataFields: true,
                showFilterFields: false,
                showRowFields: true,
                allowFieldDragging: false,
                visible: true
            }
        });  
    }
}