﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataWarehouse.Common.Data.Model.TractData
{
    public class ToDoListItem : BaseModelItem
    {
        public String Name { get; set; }

        [ForeignKey("ToDoList")]
        public Guid ToDoListId { get; set; }

        public virtual ToDoList ToDoList { get; set; }

        public ToDoListItem()
        {
        }
    }
}
