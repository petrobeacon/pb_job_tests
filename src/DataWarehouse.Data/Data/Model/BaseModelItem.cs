﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataWarehouse.Common.Data.Model
{
    public abstract class BaseModelItem : IdModelItem
    {
        [MaxLength(200)]
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        [MaxLength(200)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
