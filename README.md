# README #

This project was created as a job interview test project for software development candidates.
The purpose of this project is to mimic an existing application that will be maintained as part of the role.
If the candidate can show he is comfortable with this project, we can be confident they will quickly adopt
the real project this is based on.

This job interview test can be completed as little or as much as the applicant feels is necessary.
It is recommended that at least a code review be performed to identify the technologies involved as
this will be the basis for any follow up interviews. Do not hesitate to showcase any skills you
would like to advertise.

### What should i do? ###

1. Perform Code Review.
2. Check out, Build, Run Application
3. Add view and code to edit ToDo items for given ToDo list
4. Submit a pull request to BitBucket with your changes
5. Ensure you keep us posted on any of the above you do via e-mail

### How do I get set up? ###

* Check out project
* Download [DevExpress Trial](https://go.devexpress.com/DevexpressDownload_UniversalTrial.aspx)
* Install SQL Server Express
** Create database by running scripts in pb_job_tests\scripts folder
* Open project in visual studio
* Restore nuget packages
* Build & Run
